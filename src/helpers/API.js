import axios     from 'axios'
import {BASE_URL} from '../constants';

const client = axios.create({
  baseURL: BASE_URL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  }
});

const API = function(options) {
  const onSuccess = function(response) {
    return response.data;
  }

  const onError = function(error) {
    console.log('Request failed:', error.config);

    if (error.response) {
      console.log('Status:',  error.response.status);
      console.log('Data:',    error.response.data);
      console.log('Headers:', error.response.headers);
    } else {
      console.log('Error ', error.message);
    }

    return Promise.reject(error.response || error.message);
  }

  return client(options)
            .then(onSuccess)
            .catch(onError);
}

export default API;