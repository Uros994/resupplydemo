import {
    ZIP_CODE_CHANGED, 
    SEARCH_CHARITIES,
    LOGOUT_USER
} from '../actions/types';

const INITIAL_STATE = {
    zipCode: '',
    foundCharities: []
};

const searchInfo = (
    state = INITIAL_STATE, action
) => {
    switch (action.type) {
        case SEARCH_CHARITIES:
            return { ...state, foundCharities: action.payload }
        case ZIP_CODE_CHANGED:
            return { ...state, zipCode: action.payload };
        case LOGOUT_USER:
            return { ...state, zipCode: '', foundCharities: []};
        default:
            return state;
    }
}

export default searchInfo;