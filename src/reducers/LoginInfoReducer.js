import {
    EMAIL_CHANGED,
    PASSWORD_CHANGED,
    START_LOGIN,
    END_LOGIN,
    SET_LOGIN_ERROR,
    LOGOUT_USER
} from '../actions/types';

const INITIAL_STATE = {
    email: '',
    password: '',
    loggingInNow: false,
    loginError: false
};

const loginInfo = (
    state = INITIAL_STATE, action
) => {
    switch (action.type) {
        case START_LOGIN:
            return { ...state, loggingInNow: true, loginError: false };
        case END_LOGIN:
            return { ...state, loggingInNow: false };
        case EMAIL_CHANGED:
            return { ...state, email: action.payload };
        case PASSWORD_CHANGED:
            return { ...state, password: action.payload };
        case SET_LOGIN_ERROR:
            return { ...state, loginError: true };  
        case LOGOUT_USER:
            return { ...state, email: '', password: ''};      
        default:
            return state;
    }
}

export default loginInfo;