import { combineReducers } from 'redux';
import loginInfo from './LoginInfoReducer';
import userInfo from './CurrentUserReducer';
import searchInfo from './CharitiesReducer';

export default combineReducers({
  loginInfo,
  userInfo,
  searchInfo
});