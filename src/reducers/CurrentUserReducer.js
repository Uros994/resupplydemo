import {
    SET_USER_INFO,
    LOGOUT_USER
} from '../actions/types';

const INITIAL_STATE = {
    user: null,
    authorization: null
};

const userInfo = (
    state = INITIAL_STATE, action
) => {
    switch (action.type) {
        case SET_USER_INFO:
            return { ...state, user: action.payload.user, authorization: action.payload.roles[0].authorization };
        case LOGOUT_USER:
            return { ...state, user: null, authorization: {}};    
        default:
            return state;
    }
}

export default userInfo;