import { createStackNavigator } from 'react-navigation';
import Login from '../screens/Login';
import Charities from '../screens/Charities';

const RootStack = createStackNavigator({
    Login: {
        screen: Login,
        navigationOptions: {
            header: null
        }
    },
    Charities: {
        screen: Charities,
        navigationOptions: {
            header: null
        }
    }
});

export default RootStack;