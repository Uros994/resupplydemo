import React from 'react';
import { TextInput } from 'react-native';

const Input = ({ value, onChangeText, placeholder = '', secureTextEntry = false, customStyle }) => {
    const { inputStyle } = styles;

    return (
        <TextInput
            secureTextEntry={secureTextEntry}
            placeholder={placeholder}
            autoCorrect={false}
            style={{...inputStyle, ...customStyle}}
            value={value}
            onChangeText={onChangeText}
        />
    );
};

const styles = {
    inputStyle: {
        backgroundColor: 'rgb(242,242,242)',
        borderRadius: 10,
        paddingLeft: 15
    }
};

export { Input };