import React from 'react';
import { Text } from 'react-native';

const MyText = (props) =>
    <Text {...props} style={[{ fontFamily: 'Brown-Bold' }, props.style]}>
        {props.children}
    </Text>

export { MyText };