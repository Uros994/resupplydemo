// login types
export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const START_LOGIN = 'start_login';
export const END_LOGIN = 'end_login';
export const SET_LOGIN_ERROR = 'set_login_error';
export const LOGOUT_USER = 'logout_user';

// user info types
export const SET_USER_INFO = 'set_user_info';

// charities types
export const SEARCH_CHARITIES = 'search_charities';
export const ZIP_CODE_CHANGED = 'zip_code_changed';