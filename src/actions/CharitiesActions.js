import {
    SEARCH_CHARITIES,
    ZIP_CODE_CHANGED
} from './types';

import API from '../helpers/API';

export const searchCharities = (zipCode, token) => {
    return (dispatch) => {
        API({
            method: 'get',
            url: `/charities?zip=${zipCode}`,
            auth: { Authorization: 'Bearer ' + { token } }
        }).then((response) => {
            dispatch({
                type: SEARCH_CHARITIES,
                payload: response
            });
        })
    };
};

export const zipCodeChanged = (zipCode) => {
    return {
        type: ZIP_CODE_CHANGED,
        payload: zipCode
    };
};
