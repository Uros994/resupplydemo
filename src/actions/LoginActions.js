import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  START_LOGIN,
  END_LOGIN,
  SET_USER_INFO,
  SET_LOGIN_ERROR,
  LOGOUT_USER
} from './types';

import API from '../helpers/API';
import NavigationService from '../helpers/NavigationService';

export const emailChanged = (email) => {
  return {
    type: EMAIL_CHANGED,
    payload: email
  };
};

export const passwordChanged = (password) => {
  return {
    type: PASSWORD_CHANGED,
    payload: password
  };
};

export const logoutUser = () => {
  return {
    type: LOGOUT_USER
  };
};

export const loginUser = (email, password) => {
  return (dispatch) => {

    const data = JSON.stringify({
      email,
      password,
      scope: 'app',
      provider: 'email'
    });

    dispatch({
      type: START_LOGIN
    })

    API({
      method: 'post',
      url: '/users/authenticate',
      data: data
    }).then((response) => {

      dispatch({
        type: END_LOGIN
      });

      dispatch({
        type: SET_USER_INFO,
        payload: response
      });

      NavigationService.navigate('Charities');

    }).catch((error) => {

      dispatch({
        type: END_LOGIN
      });

      dispatch({
        type: SET_LOGIN_ERROR
      });
    });
  };
};


