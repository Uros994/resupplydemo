const styles = {
    containerStyle: {
        backgroundColor: 'rgb(81, 153, 211)',
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10
    }
};

export default styles;