const loginStyles = {
    welcomeText: {
        fontSize: 18,
        color: 'white',
        textAlign: 'center',
        marginTop: 60
    },
    welcomeLogo: {
        marginTop: 15,
        alignSelf: 'center'
    },
    formContainer: {
        marginTop: 50,
        backgroundColor: 'white',
        paddingLeft: 20,
        paddingRight: 20,
        borderRadius: 10
    },
    loginText: {
        color: 'rgb(116,116,116)',
        fontSize: 22,
        marginTop: 20
    },
    loginButton: {
        marginTop: 15,
        marginBottom: 20,
        height: 50,
        backgroundColor: 'rgb(81,153,211)',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginButtonText: {
        fontSize: 17,
        color: 'white'
    },
    loginErrorText: {
        fontSize: 14,
        fontStyle: 'italic',
        textAlign: 'center',
        color: 'rgb(116,116,116)',
        marginTop: 11,
        marginBottom: 7
    }
};

export default loginStyles;
