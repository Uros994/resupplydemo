const charitiesStyles = {
    pickText: {
        fontSize: 22,
        color: 'white',
        marginTop: 20
    },
    charityContainer: {
        borderRadius: 10,
        marginBottom: 10,
        backgroundColor: 'white'
    },
    charityImage: {
        width: '100%',
        height: 180,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        overflow: 'hidden'
    },
    charityTextContainer: {
        flex: 1,
        justifyContent: 'space-between',
        marginLeft: 10
    },
    charityHeader: {
        fontSize: 22,
        color: 'white'
    },
    starImage: {
        height: 16,
        width: 16,
        alignSelf: 'center'
    },
    donateButton: {
        marginTop: 15,
        marginBottom: 20,
        marginLeft: 10,
        marginRight: 10,
        height: 50,
        backgroundColor: 'rgb(81,153,211)',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    donateButtonText: {
        fontSize: 18,
        color: 'white'
    },
    typesContainer: {
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10,
        flexDirection: 'row', 
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    typeLogo: {
        width: 32,
        height: 32,
        alignSelf: 'center'
    },
    typeText: {
        marginTop: 1,
        fontSize: 9,
        color: 'rgb(90,90,90)'
    },
    typeTextGray: {
        marginTop: 1,
        fontSize: 9,
        color: 'rgb(216,216,216)'
    },
    messageContainer: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        marginRight: 10,
        marginTop: 15
    },
    messageText: {
        color: 'white',
        fontSize: 18
    },
    messageImage: {
        width: 32,
        height: 32
    }
};

export default charitiesStyles;
