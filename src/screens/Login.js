import React, { Component } from 'react';
import { ScrollView, View, Image, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';

import { Input, MyText } from '../common';
import { emailChanged, passwordChanged, loginUser } from '../actions';
import styles from '../styles';
import loginStyles from '../styles/loginStyles';

class Login extends Component {

    onEmailChanged(email) {
        this.props.emailChanged(email);
    }

    onPasswordChanged(password) {
        this.props.passwordChanged(password);
    }

    onLoginButtonPress() {
        const { email, password } = this.props;

        this.props.loginUser(email, password);
    }

    renderLoginError() {
        const { loginError } = this.props;

        return(loginError &&
            <MyText style={loginStyles.loginErrorText}>
                {'E-mail and password do not match'}
            </MyText> 
        );
    }

    renderLoginButton() {
        const { loggingInNow } = this.props;

        return (
                <TouchableWithoutFeedback onPress={this.onLoginButtonPress.bind(this)}>
                    <View style={loginStyles.loginButton}>
                        {loggingInNow
                            ?
                            <MyText style={loginStyles.loginButtonText}>
                                {'Trying...'}
                            </MyText>
                            :
                            <MyText style={loginStyles.loginButtonText}>
                                {'Login'}
                            </MyText>
                        }
                    </View>
                </TouchableWithoutFeedback>

        );
    }

    renderLoginForm() {
        const { email, password } = this.props;

        return (
            <View style={loginStyles.formContainer}>
                <MyText style={loginStyles.loginText}>
                    {'LOGIN'}
                </MyText>
                <Input onChangeText={this.onEmailChanged.bind(this)} value={email} customStyle={{ marginTop: 15 }} placeholder={'E-mail'} />
                <Input onChangeText={this.onPasswordChanged.bind(this)} value={password} customStyle={{ marginTop: 10 }} placeholder={'Password'} secureTextEntry={true} />
                {this.renderLoginError()}
                {this.renderLoginButton()}
            </View>
        )
    }

    render() {
        return (
            <ScrollView style={styles.containerStyle}>
                <MyText style={loginStyles.welcomeText}>
                    {'Welcome to'}
                </MyText>
                <Image style={loginStyles.welcomeLogo} source={require('../../images/resupplyLogo03.png')} />
                {this.renderLoginForm()}
            </ScrollView>
        );
    }
}

const mapStateToProps = ({ loginInfo }) => {
    const { email, password, loggingInNow, loginError } = loginInfo;

    return { email, password, loggingInNow, loginError };
};

export default connect(mapStateToProps, {
    emailChanged, passwordChanged, loginUser
})(Login);