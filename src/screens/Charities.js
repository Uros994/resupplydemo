import React, { Component } from 'react';
import { ScrollView, View, ImageBackground, Image, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import _ from 'lodash';

import { Input, MyText } from '../common';
import styles from '../styles';
import charitiesStyles from '../styles/charitiesStyles';

import { searchCharities, zipCodeChanged, logoutUser } from '../actions';
import {SITE_URL, CHARITY_LOGOS} from '../constants';

class Charities extends Component {

    constructor(props) {
        super(props);
        this.searchDelayed = _.debounce(this.searchCharities, 1000);
    }

    searchCharities() {
        const { token, zipCode} = this.props;

        this.props.searchCharities(zipCode, token);
    }

    onZipCodeChanged(zipCode) {
        const { token } = this.props;
        this.props.zipCodeChanged(zipCode);
        this.searchDelayed();
    }

    onLogoutButtonPress() {
        this.props.logoutUser();
        const backAction = NavigationActions.back({});
        this.props.navigation.dispatch(backAction);
    }

    renderLogos() {
        return (
            <View style={charitiesStyles.typesContainer}>
                <View>
                    <Image style={charitiesStyles.typeLogo} source={require('../../images/furniture.png')}/>
                    <MyText style={charitiesStyles.typeText}>{'Furniture'}</MyText>
                </View>
                <View>
                    <Image style={charitiesStyles.typeLogo} source={require('../../images/clothes.png')}/>
                    <MyText style={charitiesStyles.typeTextGray}>{'Clothes'}</MyText>
                </View>
                <View>
                    <Image style={charitiesStyles.typeLogo} source={require('../../images/electronics.png')}/>
                    <MyText style={charitiesStyles.typeText}>{'Electronics'}</MyText>
                </View>
                <View>
                    <Image style={charitiesStyles.typeLogo} source={require('../../images/appliances.png')}/>
                    <MyText style={charitiesStyles.typeText}>{'Appliances'}</MyText>
                </View>
                <View>
                    <Image style={charitiesStyles.typeLogo} source={require('../../images/household.png')}/>
                    <MyText style={charitiesStyles.typeText}>{'Household'}</MyText>
                </View>
                <View>
                    <Image style={charitiesStyles.typeLogo} source={require('../../images/books.png')}/>
                    <MyText style={charitiesStyles.typeTextGray}>{'Books'}</MyText>
                </View>
                <View>
                    <Image style={charitiesStyles.typeLogo} source={require('../../images/misc.png')}/>
                    <MyText style={charitiesStyles.typeTextGray}>{'Misc.'}</MyText>
                </View>
            </View>
        )
    }

    renderCharities() {
        const { foundCharities } = this.props;

        return (
            <View style={{marginTop: 15}}>
                {foundCharities.map((charity, index) => {
                    return(
                        <View key={index} style={charitiesStyles.charityContainer}>
                            <ImageBackground
                                style={charitiesStyles.charityImage}
                                source={{uri: SITE_URL + charity.photo.url}}
                            >
                                <View style={charitiesStyles.charityTextContainer}>
                                    <View style={charitiesStyles.messageContainer}>
                                        <MyText style={charitiesStyles.messageText}>{'Message'}</MyText>
                                        <Image style={charitiesStyles.messageImage} source={require('../../images/chat.png')} />
                                    </View>
                                    <View>
                                        <MyText style={charitiesStyles.charityHeader}>{charity.name.toUpperCase()}</MyText>
                                        <View style={{flexDirection: 'row'}}>
                                            <MyText style={charitiesStyles.charityHeader}>{charity.rating_count + ' Ratings'}</MyText>
                                            <MyText style={{...charitiesStyles.charityHeader, marginLeft: 10}}>{charity.average_rating}</MyText>
                                            <Image style={charitiesStyles.starImage} source={require('../../images/ratingStar.png')} />
                                        </View>
                                    </View>
                                </View>
                            </ImageBackground> 
                            {this.renderLogos()}
                            <View style={charitiesStyles.donateButton}>
                                <MyText style={charitiesStyles.donateButtonText}>
                                    {'DONATE'}
                                </MyText>
                            </View>
                        </View>
                    )
                })}
            </View>
        )
    }

    render() {
        const {  zipCode } = this.props;

        return (
            <ScrollView style={styles.containerStyle}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <MyText style={charitiesStyles.pickText}>
                        {'PICK A CHARITY'}
                    </MyText>
                    <TouchableWithoutFeedback onPress={this.onLogoutButtonPress.bind(this)}>
                        <View>
                            <MyText style={{...charitiesStyles.pickText, marginLeft: 10}}>
                            {'LOG OUT'}
                            </MyText>
                        </View>
                    </TouchableWithoutFeedback>

                </View>
                <Input onChangeText={this.onZipCodeChanged.bind(this)} value={zipCode} customStyle={{ marginTop: 20 }} placeholder={'Enter zip code'} />
                {this.renderCharities()}
            </ScrollView>
        );
    }
}

const mapStateToProps = ({ userInfo, searchInfo }) => {
    const { authorization } = userInfo;
    const { zipCode, foundCharities } = searchInfo;

    return { token: authorization.token, zipCode, foundCharities };
};

export default connect(mapStateToProps, {
    searchCharities, zipCodeChanged, logoutUser
})(Charities);